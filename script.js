
const teamArr = [10]; // points per day 
const tasksList = [27]; // 
const presentDate = new Date();  // let pretend team starts work at 00:00
const deadline = new Date("06/05/2020");   //set deadline


const teamPerfomance = (team, tasks, deadline) => {
    let sumTeamPointPerday = team.reduce((sum, cur) => sum + cur);
    let sumTasksPoints = tasks.reduce((sum, cur) => sum + cur);
    let spendDaysOnTasks = (sumTasksPoints / sumTeamPointPerday);
    let spendHoursOnTasks = Math.round(sumTasksPoints / sumTeamPointPerday * 8);
    let difInTime = deadline.getTime() - presentDate.getTime();
    let CalendardaysToDeadline = (difInTime / (1000 * 60 * 60 * 24)).toFixed(0);

    let workDaysToDeadline = 0;
    let workHours = 0;   //checking for weekends
    for (let i = 0; i <= CalendardaysToDeadline; i++) {
        let tomorrow = new Date();
        tomorrow.setDate(new Date().getDate() + i);
        if (tomorrow.getDay() != 0 && tomorrow.getDay() != 6) {
            workHours += 8;   //increasing workhours if its weekend
            workDaysToDeadline++; //counting workdays
        }
    }
    //console.log(spendHoursOnTasks);

    //let daysLeftBeforeDeadline = (difInTime / (1000 * 60 * 60 * 24)).toFixed(1);
    let hoursLeftBeforeDeadline = Math.round(workDaysToDeadline * 8); 

    let result = spendHoursOnTasks < workHours ? `Все задачи будут выполнены за ${Math.floor(workDaysToDeadline - spendDaysOnTasks)} дней до наступления дедлайна` : `Команде разработчиков придется потратить дополнительно ${spendHoursOnTasks - workHours} часов после дедлайна, чтобы выполнить все задачи в беклоге`

    return result;

}

console.log(teamPerfomance(teamArr, tasksList, deadline));